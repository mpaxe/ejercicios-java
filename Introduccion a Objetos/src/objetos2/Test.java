package objetos2;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Test{
	static List<Persona> listPersonas;
	static int opcion;					// Controla la finalización del programa

	public static void main(String[] args) {

        listPersonas = new ArrayList<Persona>();  
		
		ejecutaPrograma();
    }

    public static void mostrarMenu() {
		System.out.println("\n\t\t\tMENÚ");
		System.out.println("\t|======================================|");
		System.out.println("\t|1. Mostrar la lista                   |");
		System.out.println("\t|2. Añadir un elemento más             |");
		System.out.println("\t|3. Insertar elemento en la posición   |");
		System.out.println("\t|4. Buscar y borrar un elemento        |");
		System.out.println("\t|5. Borrar un elemento de una posición |");
		System.out.println("\t|6. Editar el elemento de una posición |");
		System.out.println("\t|7. Mostrar el elemento de una posición|");
		System.out.println("\t|8. LLenar lista con varios elementos  |");
		System.out.println("\t|9. Vaciar lista                       |");
		System.out.println("\t|10.Ordena la lista                    |");
		System.out.println("\t|11.Calcular IMC                       |");
		System.out.println("\t|12.Calcualr si es mayor de edad       |");
		System.out.println("\t|======================================|");
    }
    
    public static Persona generaPersona(){
        
        String nombre = Utilidades.leerTexto("Introduce el nombre: ");
        int edad = Utilidades.leerEntero("Introduce edad: ");
        char sexo = Utilidades.leerCaracter("Introduce sexo(H hombre, M mujer): ");
        double peso = Utilidades.leerDecimal("Introduce peso: ");
        double altura = Utilidades.leerDecimal("Introduce la altura: ");
        Persona p = new Persona(nombre, edad, sexo, peso, altura);

        return p;
    }

	public static void mostrarLista() {
		
		if(listPersonas.isEmpty()) {
			System.out.println("La lista está vacía");
		}else {
			for(Persona p: listPersonas){
				System.out.println(p);
				System.out.println("---------------");
			}
			System.out.println("El contenido de la lista es ["+listPersonas.size()+"]");
		}
		
	}

	public static void insertarElemento() {
		
		listPersonas.add(generaPersona());
	}

	public static void insertarElementoPosicion() {
		
		Integer pos = Utilidades.leerEntero("Introduzca la posición donde insertar: ");
		listPersonas.add((pos), generaPersona());
	}

	public static void buscarBorrarElemento() {
		String nombre = Utilidades.leerTexto("Introduce nombre: ");
		for(Persona p: listPersonas){
			if(p.getNombre().equals(nombre)){
				listPersonas.remove(p);
				break;					// Pongo un break para romper el bucle y deja de iterar sobre la
			}							// sobre la lista para que nos salate la excepción 
		}								// ConcurrentModificationException que sucede por intentar modificar
	}									// una lista por la que se está iterando.

	public static void borraElementoPosicion() {
		int pos = Utilidades.leerEntero("Introduzca la posición del elemento a borrar: ");
		listPersonas.remove((pos - 1));
	}

	public static void editaLista() {
		int pos = Utilidades.leerEntero("Introduzca la posición del elemento a modificar: ");
		listPersonas.set((pos - 1), generaPersona());
	}

	public static void mostrarElementoPosicion() {
		int pos = Utilidades.leerEntero("Introduzca posición del elemento a mostrar: ") -1;
		System.out.println(listPersonas.get(pos));
	}
	
	public static void llenarLista() {
		listPersonas.add(new Persona("Alvaro", 35, 'H', 78, 1.78));
		listPersonas.add(new Persona("Borja", 29, 'H', 74, 1.82));
		listPersonas.add(new Persona("Rocio", 26, 'M', 55, 1.70));
		listPersonas.add(new Persona("Elisabet", 29, 'M', 59, 1.72));
		listPersonas.add(new Persona("Emma", 3, 'M', 20, 1.0));
		listPersonas.add(new Persona("Jordi", 50, 'H', 83, 1.83));
		listPersonas.add(new Persona("Pilar", 60, 'M', 65, 1.70));
		listPersonas.add(new Persona("Jesus", 32, 'H', 73, 1.75));
		listPersonas.add(new Persona("Mar", 31, 'M', 80, 1.50));
		listPersonas.add(new Persona("Juan", 70, 'H', 60, 1.73));
	}
	
	public static void borrarLista() {
		listPersonas.clear();
	}
	
	public static void ordenaLista(){   
        Collections.sort(listPersonas); 
	}

	public static void resultadoIMC(){
		for(Persona p: listPersonas){
            switch (p.calcularIMC()) {
                case -1:
                    System.out.println(p.getNombre()+" infrapeso");
                    break;
                case 0:
                    System.out.println(p.getNombre()+" peso ideal");
                    break;
                case 1:
                    System.out.println(p.getNombre()+" sobrepeso");
                    break;
            }
    
            System.out.println("------------------------");
        }
	}

	public static void resultadoMayorEdad(){
		for(Persona p: listPersonas){
            if(p.esMayorDeEdad()){
                System.out.println(p.getNombre()+ " es mayor de edad");
            }else {
                System.out.println(p.getNombre()+ " es menor de edad");
            }
    
            System.out.println("------------------------");
        }    
	}

	public static void ejecutaPrograma(){
		opcion = 0;

		do {
			mostrarMenu();
			opcion = Utilidades.leerEntero("\t\tOpción(0-Salir): ");
			System.out.println();
			switch (opcion) {
			case 1:
				mostrarLista();
				break;
			case 2:
				insertarElemento();
				break;
			case 3:
				insertarElementoPosicion();
				break;
			case 4:
				buscarBorrarElemento();
				break;
			case 5:
				borraElementoPosicion();
				break;
			case 6:
				editaLista();
				break;
			case 7:
				mostrarElementoPosicion();
				break;
			case 8:
				llenarLista();
				break;
			case 9:
				borrarLista();
				break;
			case 10:
				ordenaLista();
				break;
			case 11:
				resultadoIMC();
				break;
			case 12:
				resultadoMayorEdad();
				break;
			}		
		}while(opcion!=0);
	}
  
}