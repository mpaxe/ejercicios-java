package objetos2;


public class Persona implements Comparable<Persona>{
    private String nombre;
    private int edad;
    private String dni;
    private char sexo;
    private double peso;
    private double altura;

    public Persona(){
        this.nombre = "";
        this.edad = 0;
        this.dni = generarDNI();
        this.sexo = 'H';
        this.peso = 0.0;
        this.altura = 0.0;
    }

    public Persona(String nombre, int edad, char sexo){
        this.nombre = nombre;
        this.edad = edad;
        this.dni = generarDNI();
        this.sexo = comprobarSexo(sexo);
        this.peso = 0.0;
        this.altura = 0.0;
    }

    public Persona(String nombre, int edad, char sexo, double peso, double altura){
        this.nombre = nombre;
        this.edad = edad;
        this.dni = generarDNI();
        this.sexo = comprobarSexo(sexo);
        this.peso = peso;
        this.altura = altura;
    }

    public String getNombre(){
        return this.nombre;
    }

    public int getEdad(){
        return this.edad;
    }

    public String getDNI(){
        return this.dni;
    }

    public char getSexo(){
        return this.sexo;
    }

    public double getPeso(){
        return this.peso;
    }

    public double getAltura(){
        return this.altura;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setEdad(int edad){
        this.edad = edad;
    }

    public void setSexo(char sexo){
        this.sexo = comprobarSexo(sexo);
    }

    public void setPeso(double peso){
        this.peso = peso;
    }

    public void setAltura(double altura){
        this.altura = altura;
    }

    public int calcularIMC(){
        int res;
        double imc;
        imc = peso/(Math.pow(altura, 2));
        if(imc < 20){
            res = -1;
        }else if(imc < 25){
            res = 0;
        }else {
            res = 1;
        }
        return res;
    }

    public boolean esMayorDeEdad(){
        boolean res = false;
        if(edad >= 18){
            res = true;
        }
        return res;
    }

    public char comprobarSexo(char sexo){
        char sexComprobado;
        switch(sexo){
            case 'H':
                sexComprobado = 'H';
                break;
            case 'M':
                sexComprobado = 'M';
                break;
            default:
                sexComprobado = 'H';
                break;    
        }
        return sexComprobado;
    }

    public String toString(){
        return "Nombre: "+this.nombre+
               "\nEdad  : "+this.edad+
               "\nDNI   : "+this.dni+
               "\nSexo  : "+this.sexo+
               "\nPeso  : "+this.peso+
               "\nAltura: "+this.altura;
    }

    public int compareTo(Persona p){
        if(getEdad() < p.getEdad()){
            return -1;
        }else if(getEdad()> p.getEdad()){
            return 1;
        }
        return 0;
    }

    public String generarDNI(){
        int dniEntero;
        dniEntero = (int)(Math.random() * 100000000);   
        return this.dni = Integer.toString(dniEntero);
    }

}