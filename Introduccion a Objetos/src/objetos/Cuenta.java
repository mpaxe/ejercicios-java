package objetos;

public class Cuenta {
    private String titular;
    private double cantidad;

    public Cuenta(String titular, int cantidad){
        this.titular = titular;
        this.cantidad = cantidad;
    }

    public String getTitular(){
        return this.titular;
    }

    public double getCantidad(){
        return this.cantidad;
    }

    public void setTitular(String titular){
        this.titular = titular;
    }

    public void setCantidad(double cantidad){
        this.cantidad = cantidad;
    }

    public String toString(){
        return this.titular +": "+this.cantidad;
    }

    public void ingresar(double ingreso){
        if (ingreso > 0){
            cantidad += ingreso;
        }
    }

    public void retirar(double retirada){
        if((cantidad - retirada) < 0){
            cantidad = 0;
        }else{
            cantidad -= retirada;
        }
    }

    public void nuevoTitular(String nuevoTitular) {
        setTitular(nuevoTitular);
    }
}
