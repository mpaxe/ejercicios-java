package objetos;

public class Test {

	public static void main(String[] args) {
		Cuenta c1 = new Cuenta("Alvaro Vera", 500);
		

		System.out.println(c1.getCantidad());
		c1.ingresar(100);
		System.out.println(c1.getCantidad());

		c1.nuevoTitular("Borja Vera");
		System.out.println(c1.getTitular());

		c1.retirar(200);
		System.out.println(c1.getCantidad());
		
	}

}
